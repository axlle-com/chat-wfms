function message(text){
    let message = `<div><div class="chat-message">${text}</div></div>`;
    $('#chat-result').append(message);
}

function socketCustom(){
    const socket = new WebSocket('ws://chat-wfms.loc:8000/server.php?uuid=007788');
    socket.onopen = function(e) {
        message('Соединение установлено...');
        $('.js-socket-open').hide();
    };
    socket.onerror = function(error) {
        message(error.message ? error.message : 'Произошла ошибка');
    };
    socket.onclose = function(event) {
        if (event.wasClean) {
            message(`Соединение закрыто чисто, код=${event.code} причина=${event.reason}`);
        } else {
            // например, сервер убил процесс или сеть недоступна обычно в этом случае event.code 1006
            message('Соединение прервано');
        }
        $('.js-socket-open').show();
    };
    socket.onmessage = function(event) {
        let data = JSON.parse(event.data);
        message(`${data.type} - ${data.message}`);
    };
}

$(document).ready(function () {
    $('body').on('click','.js-socket-open',function (e) {
        e.preventDefault();
        socketCustom();
    })

});