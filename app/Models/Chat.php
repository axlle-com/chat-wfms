<?php

namespace App\Models;

class Chat
{
    public function sendHeaders($headersText,$newSocket,$host,$port): bool
    {
        $info = [];
        $lines = explode("\r\n",$headersText);
        foreach ($lines as $i => $line) {
            if ($i) {
                if (preg_match('/\A(\S+): (.*)\z/', $line, $matches)) {
                    $info[$matches[1]] = $matches[2];
                }
            } else {
                $header = explode(' ', $line);
                $info['method'] = $header[0];
                $info['uri'] = $header[1];
            }
            if (empty(trim($line))) {
                break;
            }
        }
        if (empty($info['Sec-WebSocket-Key'])) {
            return false;
        }

        $sKey = base64_encode(pack('H*',sha1($info['Sec-WebSocket-Key'].'258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));
        $strHeader = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n".
            "Upgrade: websocket\r\n".
            "Connection: Upgrade\r\n".
            "WebSocket-Origin: $host\r\n".
            "WebSocket-Location: ws://$host:$port/server.php\r\n".
            "Sec-WebSocket-Accept: $sKey\r\n\r\n";

        socket_write($newSocket,$strHeader,strlen($strHeader));

        return true;
    }

    public function handshake($connect): bool
    {
        $info = [];
        $data = socket_read($connect, 1000);
        $lines = explode("\r\n", $data);
        foreach ($lines as $i => $line) {
            if ($i) {
                if (preg_match('/\A(\S+): (.*)\z/', $line, $matches)) {
                    $info[$matches[1]] = $matches[2];
                }
            } else {
                $header = explode(' ', $line);
                $info['method'] = $header[0];
                $info['uri'] = $header[1];
            }
            if (empty(trim($line))) break;
        }

        // получаем адрес клиента
        $ip = $port = null;
        if ( ! socket_getpeername($connect, $ip, $port)) {
            return false;
        }
        $info['ip'] = $ip;
        $info['port'] = $port;

        if (empty($info['Sec-WebSocket-Key'])) {
            return false;
        }

        // отправляем заголовок согласно протоколу вебсокета
        $SecWebSocketAccept = base64_encode(pack('H*', sha1($info['Sec-WebSocket-Key'] . '258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));
        $upgrade = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" .
            "Upgrade: websocket\r\n" .
            "Connection: Upgrade\r\n" .
            "Sec-WebSocket-Accept:".$SecWebSocketAccept."\r\n\r\n";
        socket_write($connect, $upgrade);
        return true;
    }

    public function newConnectionACK(?string $clientIp = null): string
    {
        $message = 'New client '. ($clientIp ?? '') .' connected';
        $messageArray = [
            'message' => $message,
            'type' => 'newConnectionACK'
        ];
        return $this->seal(json_encode($messageArray));
    }

    public function seal($socketData): string
    {
        $b1 = 0x81;
        $length = strlen($socketData);
        $header = '';

        if($length <= 125){
            $header = pack('CC',$b1,$length);
        }
        elseif ($length > 125 && $length < 65536){
            $header = pack('CCn',$b1,126,$length);
        }
        elseif($length > 65535){
            $header = pack('CCNN',$b1,127,$length);
        }
        return $header.$socketData;
    }

    public function send($message,$clientSocketArray): bool
    {
        $messageLength = strlen($message);
        foreach ($clientSocketArray as $clientSocket){
            @socket_write($clientSocket,$message,$messageLength);
        }
        return true;
    }


}
