<?php

require __DIR__.'/vendor/autoload.php';

use App\Models\Chat;

const PORT = '8000';
const HOST = '127.0.0.1';

$chat = new Chat();

$socket = socket_create(AF_INET,SOCK_STREAM,SOL_TCP);
$bind = socket_bind($socket,HOST,PORT);
$option = socket_set_option($socket,SOL_SOCKET,SO_REUSEADDR,1);
$listen = socket_listen($socket);

$clientSocketArray[] = $socket;

while (true){
    if ($socket === false) {
        echo 'Error socket_create(): ' . socket_strerror(socket_last_error());
        return;
    }
    if ($bind === false) {
        echo 'Error socket_bind(): ' . socket_strerror(socket_last_error());
        return;
    }
    if ($option === false) {
        echo 'Error socket_set_option(): ' . socket_strerror(socket_last_error());
        return;
    }
    if ($listen === false) {
        echo 'Error socket_listen(): ' . socket_strerror(socket_last_error());
        return;
    }
    echo 'Server is running...'. PHP_EOL;

    $newSocketArray = $clientSocketArray;

    $write = $except = null;
    if (!socket_select($newSocketArray, $write, $except, null)) { // ожидаем сокеты, доступные для чтения (без таймаута)
        break;
    }
    if (in_array($socket, $newSocketArray, true)) {
        if (($newSocket = socket_accept($socket)) && $chat->handshake($newSocket)) {
            echo '[' . date('d.m.Y H:i:s') . '] ' . 'Open connect' . PHP_EOL;
            $clientSocketArray[] = $newSocket; // добавляем его в список необходимых для обработки

            $connectionACK = $chat->newConnectionACK();
            $chat->send($connectionACK,$clientSocketArray);
        }
    }
}
socket_close($socket);